package rob.myappcompany.currencyconvertapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    public void currencyConver(View view){
        Log.i("Info", "Button Pressed!");
        EditText editText = (EditText) findViewById(R.id.editText2);

        String amountPesos = editText.getText().toString();

        double amountPesosDouble = Double.parseDouble(amountPesos);
        double amountDollarDouble = amountPesosDouble * 63.3;

        //String amountDollarString = Double.toString(amountDollarDouble);
        String amountDollarString = String.format("%.2f", amountDollarDouble);

        Toast.makeText(this, "$"+ amountPesos + " is USD" + amountDollarString,
                Toast.LENGTH_LONG).show();

        Log.i("Info", editText.getText().toString());
        Log.i("Amount in Dollars", amountDollarString);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
